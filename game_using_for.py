from random import randint

name = input("Hello. What is your name? ")
print("Hi,", name + "!")

for guess_number in range(1,6):
    month = randint(1,12)
    year = randint(1924, 2004)

    guess = print("Is your birth month and year:", month, "/", year,"?")
    response = input("Did I guess it right? Yes or No: ")

    if response == "Yes":
        print("I knew it!")
        break
    elif guess_number == 5:
        print("I have other things to do. Good by.")
    else:
        print("Dang! Let me try again!")
